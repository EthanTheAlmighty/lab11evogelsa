﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

public class threadedMovement : MonoBehaviour {

    float timer = 0.0f;
    Vector3 travelDirection;
    Vector3 newPos;

	// Update is called once per frame
	void Update ()
    {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            NewDir();

            timer = 5.0f;
            Thread temp = new Thread(ThreadedMovement);
            temp.Start();   
        }
	}

    void LateUpdate()
    {
        transform.Translate(travelDirection);
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.GetComponent<HealthScript>())
        {
            col.gameObject.GetComponent<HealthScript>().health--;
        }
    }

    void ThreadedMovement()
    {
        //this doesn't work, i get told to take my shit back to the main thread
        //transform.Translate(travelDirection);
        travelDirection = newPos;
    }

    void NewDir()
    {
        newPos = Random.insideUnitSphere * 0.1f;
    }
}
