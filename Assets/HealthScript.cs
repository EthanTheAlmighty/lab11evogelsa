﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour
{
    public int health = 5;

    Text healthIndicator;

    void Start()
    {
        healthIndicator = GameObject.Find("Health Indicator Text").GetComponent<Text>();
    }

    void Update()
    {
        healthIndicator.text = "Health: " + health.ToString();
    }

    //how do i translate outside of fixedupdate, Time.Time + stuff?
    //how far am i moving the cubes, in what direction? do i use the random unit sphere?

}
